select RequestDate, ProjectID, ChangeRequestID, ChangeRequestTitle, UnitRequester, UserRequest,
 Status, LastAssignUnitTo, PIC, FinalDate from NewPMIS_ChangeRequest
 
 select c.RequestDate, c.ProjectID, c.ChangeRequestID, c.ChangeRequestTitle, 
 c.UnitRequester, b.EmployeeName RequestName,
 d.Nama analisName, DATEDIFF(day,isnull(FinalDate,getdate()), c.RequestDate) Aging , 
 c.Status, c.LastAssignUnitTo CurrentPosition, c.PIC, t.StartDate, c.FinalDate
 from NewPMIS_ChangeRequest c 
 left join BFIDB..BranchEmployee b on c.UserRequest = b.EmployeeID
 left join NewPMIS_trProjectPlanDetail t on t.ProjectID = c.ProjectID and t.ChangeRequestID = c.ChangeRequestID
 left join DTEmployee d on d.NoPeg = t.AssignTo
 where t.PlanID = 1


declare @temptable as table
(
	id int identity,
	requestdate date,
	projectID int,
	CRNo varchar(50),
	title varchar(100),
	unitrequester varchar(50),
	RequestName varchar(50),
	analistName varchar(50),	
	Aging int, 
	status varchar(50),
	CurrentPosition varchar (50),
	PIC varchar(50),
	stardate date,
	finaldate date	
)

insert into @temptable(
	requestdate, projectID, CRNo, title, 
	unitrequester, requestname, analistname, aging, status,
	currentposition, pic, stardate, finaldate
) 
 select c.RequestDate, c.ProjectID, c.ChangeRequestID, c.ChangeRequestTitle, 
 c.UnitRequester, b.EmployeeName RequestName,
 d.Nama analisName, DATEDIFF(day,isnull(FinalDate,getdate()), c.RequestDate) Aging , 
 c.Status, c.LastAssignUnitTo CurrentPosition, c.PIC, t.StartDate, c.FinalDate
 from NewPMIS_ChangeRequest c 
 left join BFIDB..BranchEmployee b on c.UserRequest = b.EmployeeID
 left join NewPMIS_trProjectPlanDetail t on t.ProjectID = c.ProjectID and t.ChangeRequestID = c.ChangeRequestID
 left join DTEmployee d on d.NoPeg = t.AssignTo
 where t.PlanID = 1
 
 select * from @temptable
 
 create procedure NewPMIS_MsVendor
  @search varchar(50)
 as
 begin
 declare @temptable as table
(
	id int identity,
	requestdate date,
	projectID int,
	CRNo varchar(50),
	title varchar(100),
	unitrequester varchar(50),
	RequestName varchar(50),
	analistName varchar(50),	
	Aging int, 
	status varchar(50),
	CurrentPosition varchar (50),
	PIC varchar(50),
	stardate date,
	finaldate date	
)

insert into @temptable(
	requestdate, projectID, CRNo, title, 
	unitrequester, requestname, analistname, aging, status,
	currentposition, pic, stardate, finaldate
) 
 select c.RequestDate, c.ProjectID, c.ChangeRequestID, c.ChangeRequestTitle, 
 c.UnitRequester, b.EmployeeName RequestName,
 d.Nama analisName, DATEDIFF(day,isnull(FinalDate,getdate()), c.RequestDate) Aging , 
 c.Status, c.LastAssignUnitTo CurrentPosition, c.PIC, t.StartDate, c.FinalDate
 from NewPMIS_ChangeRequest c 
 left join BFIDB..BranchEmployee b on c.UserRequest = b.EmployeeID
 left join NewPMIS_trProjectPlanDetail t on t.ProjectID = c.ProjectID and t.ChangeRequestID = c.ChangeRequestID
 left join DTEmployee d on d.NoPeg = t.AssignTo
 where t.PlanID = 1
 
 select * from @temptable
 end
 
 
 -------------------------------------------------------------------------
 select c.RequestDate, c.ProjectID, c.ChangeRequestID, c.ChangeRequestTitle, 
			c.UnitRequester, b.EmployeeName RequestName,
			d.Nama analisName, DATEDIFF(day,isnull(FinalDate,getdate()), c.RequestDate) Aging , 
			c.Status, c.LastAssignUnitTo CurrentPosition, c.PIC, t.StartDate, c.FinalDate
		 from NewPMIS_ChangeRequest c 
		 left join BFIDB..BranchEmployee b on c.UserRequest = b.EmployeeID
		 left join NewPMIS_trProjectPlanDetail t on t.ProjectID = c.ProjectID and t.ChangeRequestID = c.ChangeRequestID
		 left join DTEmployee d on d.NoPeg = t.AssignTo
		 where t.PlanID = 1 
 and c.RequestDate between '11/01/2016' and '09/30/2017' 
 and b.EmployeeName like '%%'
 
 --------------------------------------------------------------
 
 
 